package br.ufpr.tads.activitylifecycle

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.i("DS151","Tela1::onCreate")
        button1.setOnClickListener {
            val intent = Intent(this, Main2Activity::class.java)
            startActivity(intent)
        }
    }

    override fun onStart(){
        super.onStart()
        Log.i("DS151","Tela1::onStart")
    }

    override fun onPause(){
        super.onPause()
        Log.i("DS151","Tela1::onPause")
    }

    override fun onResume(){
        super.onResume()
        Log.i("DS151","Tela1::onResume")
    }

    override fun onRestart(){
        super.onRestart()
        Log.i("DS151","Tela1::onRestart")
    }

    override fun onStop(){
        super.onStop()
        Log.i("DS151","Tela1::onStop")
    }

    override fun onDestroy(){
        super.onDestroy()
        Log.i("DS151","Tela1::onDestroy")
    }
}
