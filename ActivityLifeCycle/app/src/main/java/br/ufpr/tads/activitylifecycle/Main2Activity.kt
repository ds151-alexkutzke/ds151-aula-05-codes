package br.ufpr.tads.activitylifecycle

import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main2.*

class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        Log.i("DS151","Tela2::onCreate")

        button2.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onStart(){
        super.onStart()
        Log.i("DS151","Tela2::onStart")
    }

    override fun onPause(){
        super.onPause()
        Log.i("DS151","Tela2::onPause")
    }

    override fun onResume(){
        super.onResume()
        Log.i("DS151","Tela2::onResume")
    }

    override fun onRestart(){
        super.onRestart()
        Log.i("DS151","Tela2::onRestart")
    }

    override fun onStop(){
        super.onStop()
        Log.i("DS151","Tela2::onStop")
    }

    override fun onDestroy(){
        super.onDestroy()
        Log.i("DS151","Tela2::onDestroy")
    }
}
