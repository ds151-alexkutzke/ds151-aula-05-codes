Tecnologia em Análise e Desenvolvimento de Sistemas

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

DS151 - Desenvolvimento para Dispositivos Móveis

Prof. Alexander Robert Kutzke

# DS151 - Aula - 05 - Codes - Activity e Intent (cont.)

Códigos apresentados na aula 05 (Activity e Intent) de DS151:

- ActivityLifeCycle com ciclo de vida de uma `Activity`;
- SalvandoEstado com manutenção de estado em uma `Activity` e definição de layout específico para posição paisagem (*landscape*);

